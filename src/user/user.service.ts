import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entities';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  getUser(id: number) {
    return this.userRepo.findOne(id);
  }

  createUser(body: any) {
    const newUser = this.userRepo.create(body);
    return this.userRepo.save(newUser);
  }

  async updateUser(id: number, body: any) {
    const user = await this.userRepo.findOne(id);
    this.userRepo.merge(user, body);
    return this.userRepo.save(user);
  }

  async deleteUser(id: number) {
    await this.userRepo.delete(id);
    return true;
  }
}
