import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MovieService } from './movie.service';
import { MovieController } from './movie.controller';
import { Movie } from './movies.entities';

@Module({
  imports: [TypeOrmModule.forFeature([Movie])],

  providers: [MovieService],
  controllers: [MovieController],
})
export class MovieModule {}
