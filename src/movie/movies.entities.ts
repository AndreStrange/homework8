import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Movie {
  @PrimaryGeneratedColumn()
  id_movie: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  poster: string;

  @Column()
  stock: number;

  @Column()
  trailer: string;

  @Column()
  price: number;

  @Column()
  likes: number;

  @Column()
  availability: boolean;
}
