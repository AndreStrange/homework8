import {
  Controller,
  Param,
  Body,
  Get,
  Post,
  Put,
  Delete,
} from '@nestjs/common';
import { MovieService } from './movie.service';

@Controller('api/movie')
export class MovieController {
  constructor(private movieService: MovieService) {}

  @Get(':id')
  getMovie(@Param('id') id: number) {
    return this.movieService.getMovie(id);
  }

  @Post()
  createMovie(@Body() body: any) {
    return this.movieService.createMovie(body);
  }

  @Put(':id')
  updateUser(@Param('id') id: number, @Body() body: any) {
    return this.movieService.updateMovie(id, body);
  }

  @Delete(':id')
  deleteUser(@Param('id') id: number) {
    return this.movieService.deleteMovie(id);
  }
}
