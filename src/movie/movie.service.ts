import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Movie } from './movies.entities';

@Injectable()
export class MovieService {
  constructor(@InjectRepository(Movie) private movieRepo: Repository<Movie>) {}

  getMovie(id: number) {
    return this.movieRepo.findOne(id);
  }

  createMovie(body: any) {
    const newUser = this.movieRepo.create(body);
    return this.movieRepo.save(newUser);
  }

  async updateMovie(id: number, body: any) {
    const movie = await this.movieRepo.findOne(id);
    this.movieRepo.merge(movie, body);
    return this.movieRepo.save(movie);
  }

  async deleteMovie(id: number) {
    await this.movieRepo.delete(id);
    return true;
  }
}
